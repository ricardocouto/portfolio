"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from profiles.api import UserViewSet, ProfileViewSet, TeamViewSet
from profiles.views import CustomAuthToken
from matches.api import MatchViewSet, FrameViewSet, BreakViewSet, BallViewSet, MatchTeamViewSet, FrameTeamProfileViewSet
from competitions.views import CompetitionViewSet, StageViewSet, PhaseViewSet, PhaseTransitionViewSet, PhaseTiebreakerViewSet, PhaseRankingPointsViewSet

from rest_framework.authtoken import views

from rest_framework import routers
from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Snooker Pocket API')

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()

router.register(r'users', UserViewSet)
router.register(r'profiles', ProfileViewSet)
router.register(r'teams', TeamViewSet)

router.register(r'competitions', CompetitionViewSet)
router.register(r'stages', StageViewSet)
router.register(r'phases', PhaseViewSet)
router.register(r'phase_transitions', PhaseTransitionViewSet)
router.register(r'phase_tiebreakers', PhaseTiebreakerViewSet)
router.register(r'phase_ranking_points', PhaseRankingPointsViewSet)

router.register(r'matches', MatchViewSet, basename='match')
router.register(r'frames', FrameViewSet)
router.register(r'breaks', BreakViewSet)
router.register(r'balls', BallViewSet)

router.register(r'match_teams', MatchTeamViewSet)
router.register(r'frame_team_profiles', FrameTeamProfileViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(router.urls)),
    path('api/auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/token-auth/', CustomAuthToken.as_view()),
    url(r'^$', schema_view),
]