from django.db.models import Avg, Sum, Min, Max, Q, F, Count, Value as V, IntegerField
from django.db.models.functions import Round, Coalesce

class Statistics():
    @property
    def best_frame(self):
        if hasattr(self, 'frames'):
            if self.frames.count():
                return max(self.frames.all(), key=lambda f: f.rating)

    @property
    def highest_break(self):
        if hasattr(self, 'breaks'):
            if self.breaks.count():
                return max(self.breaks.all(), key=lambda b: b.score)

    @property
    def top_shot(self):
        if hasattr(self, 'balls'):
            if self.balls.count():
                return max(self.balls.all(), key=lambda b: b.rating)

    @property
    def analysis(self):
        def _ratio_score(winner_stat, loser_stat, weight=1):
            ratio = winner_stat / (winner_stat + loser_stat)
            return ratio * weight

        def _absolute_score(winner_stat, loser_stat, weight=1, total=1):
            return (winner_stat - loser_stat) * weight / total

        analysis = {}
        
        try:
            winner_statistics = self.teams.filter(team=self.winner).first().statistics
            loser_statistics = self.teams.filter(team=self.loser).first().statistics

            statistical_weights = {
                'absolute': {
                    'points.color_after_red_points': 2,
                    'points.fouls_received_points': 5,
                },
                'relative': {
                    'points.final_sequence_points': 1.5,
                    'breaks.average_break': 1,
                    'points.average_color_after_red_points': 0.85,
                    'points.average_final_sequence_points': 0.85,
                },
            }

            for ratio_type in statistical_weights:
                for stat in statistical_weights[ratio_type]:
                    key = stat.split('.')
                    winner_stat = winner_statistics[key[0]][key[1]]
                    loser_stat = loser_statistics[key[0]][key[1]]

                    if winner_stat > loser_stat:
                        if ratio_type == 'relative':
                            total = winner_stat + loser_stat
                        else:
                            total = winner_statistics['points']['total_points'] + loser_statistics['points']['total_points']

                        if total > 0:
                            value = int(round(winner_stat * 100 / total, 0))
                            score = (winner_stat * statistical_weights[ratio_type][stat]) / total
                            analysis[key[1]] = value


            if hasattr(self, 'frames'):
                win_diffs = [0, 0]
                for frame in self.frames.all():
                    win_diffs[0 if frame.winner == self.winner else 1] += frame.score_difference
                
                if win_diffs[0] < win_diffs[1]:
                    analysis['frame_win_difference'] = (win_diffs[1] - win_diffs[0]) / self.frames.count()
        except:
            pass

        return dict(sorted(analysis.items(), key=lambda x: -x[1])[:3])

    @property
    def break_statistics(self):
        if hasattr(self, 'breaks'):
            return self.breaks.annotate(num_balls=Count('balls')).aggregate(
                total_breaks = Coalesce(Count('id'), V(0)),
                highest_break = Coalesce(Max('score'), V(0)),
                average_break = Round(Coalesce(Avg('score', filter=Q(score__gt=0)), V(0.0))),
                zero_value_breaks = Count('id', filter=Q(score=0)),
                breaks_over_1_ball = Count('id', filter=Q(num_balls__gt=2)),
                below_10 = Coalesce(Count('id', filter=Q(score__gte=1, score__lte=9)), V(0)),
                above_10 = Coalesce(Count('id', filter=Q(score__gte=10)), V(0)),
                above_20 = Coalesce(Count('id', filter=Q(score__gte=20)), V(0)),
                above_30 = Coalesce(Count('id', filter=Q(score__gte=30)), V(0)),
                above_40 = Coalesce(Count('id', filter=Q(score__gte=40)), V(0)),
                above_50 = Coalesce(Count('id', filter=Q(score__gte=50)), V(0)),
            )
        return {}

    @property
    def shot_statistics(self):
        if hasattr(self, 'balls'):
            return self.balls.aggregate(
                total_shots = Coalesce(Count('id'), V(0)),
                shots_potted = Coalesce(Count('id', filter=Q(value__gt=0, is_foul=False)), V(0)),
                zero_value_shots = Coalesce(Count('id', filter=Q(value=0)), V(0)),
                reds_potted = Coalesce(Count('id', filter=Q(value=1)), V(0)),
                color_after_red_potted = Coalesce(Count('id', filter=Q(value__gt=1, is_final_sequence=False, is_foul=False)), V(0)),
                final_sequence_potted = Coalesce(Count('id', filter=Q(is_final_sequence=True, is_foul=False)), V(0)),
                fouls_received = Coalesce(Count('id', filter=Q(is_foul=True)), V(0)),
            )
        return {}

    @property
    def points_statistics(self):
        if hasattr(self, 'balls'):
            return self.balls.aggregate(
                total_points = Coalesce(Sum('value'), V(0)),
                reds_points = Coalesce(Sum('value', filter=Q(value=1, is_final_sequence=False, is_foul=False, is_free_ball=False)), V(0)),
                color_after_red_points = Coalesce(Sum('value', filter=Q(value__gt=1, is_final_sequence=False, is_foul=False, is_free_ball=False)), V(0)),
                average_color_after_red_points = Round(Coalesce(Avg('value', filter=Q(value__gt=1, is_final_sequence=False, is_foul=False, is_free_ball=False)), V(0.0))),
                free_ball_points = Coalesce(Sum('value', filter=Q(is_free_ball=True)), V(0)),
                average_free_ball_points = Round(Coalesce(Avg('value', filter=Q(is_free_ball=True)), V(0.0))),
                final_sequence_points = Coalesce(Sum('value', filter=Q(is_final_sequence=True)), V(0)),
                average_final_sequence_points = Round(Coalesce(Avg('value', filter=Q(is_final_sequence=True)), V(0.0))),
                fouls_received_points = Coalesce(Sum('value', filter=Q(is_foul=True)), V(0)),
                average_fouls_received_points = Round(Coalesce(Avg('value', filter=Q(is_foul=True)), V(0.0))),
                manual_points = Coalesce(Sum('value', filter=Q(is_manual=True)), V(0)),
            )


    @property
    def statistics(self):
        return {
            'points': self.points_statistics,
            'breaks': self.break_statistics, 
            'shots': self.shot_statistics, 
        }
    
    @property
    def performance(self):
        potting = self.shot_statistics['shots_potted'] / self.shot_statistics['total_shots']
        positional_play = self.breaks.annotate(num_balls=Coalesce(Count('balls'),V(0))).aggregate(
            balls=Coalesce(Sum('num_balls', filter=Q(num_balls__gt=2))-Sum(2, filter=Q(num_balls__gt=2)),V(0))
        )['balls'] / self.shot_statistics['total_shots']
        safety = self.frame.breaks.filter(~Q(team=self.team), score=0).count() / self.breaks.count()

        return {
            'potting': potting,
            'positional_play': positional_play,
            'safety' : safety,
        }

    
    class Meta:
        abstract = True