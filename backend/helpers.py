from django_restql.mixins import DynamicFieldsMixin, QueryArgumentsMixin
from rest_framework import serializers

class DefaultSerializer(DynamicFieldsMixin, QueryArgumentsMixin, serializers.ModelSerializer):
    pass