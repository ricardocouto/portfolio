from matches.models import Match, Frame, Break, Ball
from django.db.models import Q, Avg

# MATCHES PROFILE

all_profile_matches = Match.objects.filter(matchteam__team__in=profile.teams.all())

profile_matches_won = Match.objects.filter(winner__in=profile.teams.all())

profile_matches_lost = Match.objects.filter(loser__in=profile.teams.all())

profile_matches_played = Match.objects.filter(Q(winner__in=profile.teams.all()) | Q(loser__in=profile.teams.all()))

profile_matches_win_ratio = profile_matches_won.count() * 100 / profile_matches_played.count()

# MATCHES TEAM: Replace "__in" by "", use "team" instead of "profile.teams.all()"

# FRAMES PROFILE: Use "Frame" instead of "Match"

# FRAMES TEAM: Replace "__in" by "", use "Frame" instead of "Match"

# BREAKS PROFILE

all_profile_breaks = Break.objects.filter(team__in=profile.teams.all())

profile_top_10_breaks = all_profile_breaks.order_by('-score')[:10]

profile_break_average = all_profile_breaks.aggregate(Avg('score'))

profile_0_score_breaks = all_profile_breaks.exclude(score__gt=0)

profile_10_score_breaks = all_profile_breaks.exclude(score__lte=0, score__gt=10)


# BALLS PROFILE

profile_all_balls = Ball.objects.filter(team__in=profile.teams.all())

profile_valid_balls = profile_all_balls.filter(is_foul=False, is_red_illegal_pot=False)

profile_potted_balls = profile_valid_balls.filter(value__gte=1)

profile_reds_potted = profile_valid_balls.filter(value=1)

profile_color_potted = profile_valid_balls.filter(value__gt=1, is_final_sequence=False)

potting_percentage = profile_potted_balls.count() * 100 / profile_all_balls.count()

color_after_red_percentage = profile_color_potted.count() * 100 / profile_reds_potted.count()

# BREAKS ALL TIME: Remove filter