from django.contrib import admin
from matches.models import Match, Frame, Break, Ball, MatchTeam, FrameTeamProfile

admin.site.register(Match)
admin.site.register(Frame)
admin.site.register(Break)
admin.site.register(Ball)
admin.site.register(MatchTeam)
admin.site.register(FrameTeamProfile)
