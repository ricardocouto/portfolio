from django.db import models
from profiles.models import Profile, Team
from competitions.models import Phase
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.db.models import Avg, Sum, Min, Max, Q, F, Count, Value as V, IntegerField
from django.db.models.functions import Round, Coalesce
from django.contrib.postgres.fields import JSONField
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import json
from rest_framework.reverse import reverse as api_reverse
from django.db.models import Func
import statistics



class Round(Func):
    function = "ROUND"
    template = "%(function)s(%(expressions)s::numeric, 1)"

class Statistics(models.Model):
    @property
    def best_frame(self):
        if hasattr(self, 'frames'):
            if self.frames.count():
                return max(self.frames.all(), key=lambda f: f.rating)

    @property
    def highest_break(self):
        if hasattr(self, 'breaks'):
            if self.breaks.count():
                return max(self.breaks.all(), key=lambda b: b.score)

    @property
    def top_shot(self):
        if hasattr(self, 'balls'):
            if self.balls.count():
                return max(self.balls.all(), key=lambda b: b.rating)

    @property
    def analysis(self):
        def _ratio_score(winner_stat, loser_stat, weight=1):
            ratio = winner_stat / (winner_stat + loser_stat)
            return ratio * weight

        def _absolute_score(winner_stat, loser_stat, weight=1, total=1):
            return (winner_stat - loser_stat) * weight / total

        analysis = {}
        
        try:
            winner_statistics = self.teams.filter(team=self.winner).first().statistics
            loser_statistics = self.teams.filter(team=self.loser).first().statistics

            statistical_weights = {
                'absolute': {
                    'points.color_after_red_points': 2,
                    'points.fouls_received_points': 5,
                },
                'relative': {
                    'points.final_sequence_points': 1.5,
                    'breaks.average_break': 1,
                    'points.average_color_after_red_points': 0.85,
                    'points.average_final_sequence_points': 0.85,
                },
            }

            for ratio_type in statistical_weights:
                for stat in statistical_weights[ratio_type]:
                    key = stat.split('.')
                    winner_stat = winner_statistics[key[0]][key[1]]
                    loser_stat = loser_statistics[key[0]][key[1]]

                    if winner_stat > loser_stat:
                        if ratio_type == 'relative':
                            total = winner_stat + loser_stat
                        else:
                            total = winner_statistics['points']['total_points'] + loser_statistics['points']['total_points']

                        if total > 0:
                            value = int(round(winner_stat * 100 / total, 0))
                            score = (winner_stat * statistical_weights[ratio_type][stat]) / total
                            analysis[key[1]] = value


            if hasattr(self, 'frames'):
                win_diffs = [0, 0]
                for frame in self.frames.all():
                    win_diffs[0 if frame.winner == self.winner else 1] += frame.score_difference
                
                if win_diffs[0] < win_diffs[1]:
                    analysis['frame_win_difference'] = (win_diffs[1] - win_diffs[0]) / self.frames.count()
        except:
            pass

        return dict(sorted(analysis.items(), key=lambda x: -x[1])[:3])


    def _break_statistics(self):
        if hasattr(self, 'breaks'):
            return self.breaks.annotate(num_balls=Count('balls')).aggregate(
                total_breaks = Coalesce(Count('id'), V(0)),
                highest_break = Coalesce(Max('score'), V(0)),
                average_break = Round(Coalesce(Avg('score', filter=Q(score__gt=0)), V(0.0))),
                zero_value_breaks = Count('id', filter=Q(score=0)),
                breaks_over_1_ball = Count('id', filter=Q(num_balls__gt=2)),
                below_10 = Coalesce(Count('id', filter=Q(score__gte=1, score__lte=9)), V(0)),
                above_10 = Coalesce(Count('id', filter=Q(score__gte=10)), V(0)),
                above_20 = Coalesce(Count('id', filter=Q(score__gte=20)), V(0)),
                above_30 = Coalesce(Count('id', filter=Q(score__gte=30)), V(0)),
                above_40 = Coalesce(Count('id', filter=Q(score__gte=40)), V(0)),
                above_50 = Coalesce(Count('id', filter=Q(score__gte=50)), V(0)),
            )
        return {}

    def _shot_statistics(self):
        if hasattr(self, 'balls'):
            return self.balls.aggregate(
                total_shots = Coalesce(Count('id'), V(0)),
                shots_potted = Coalesce(Count('id', filter=Q(value__gt=0, is_foul=False)), V(0)),
                zero_value_shots = Coalesce(Count('id', filter=Q(value=0)), V(0)),
                reds_potted = Coalesce(Count('id', filter=Q(value=1)), V(0)),
                color_after_red_potted = Coalesce(Count('id', filter=Q(value__gt=1, is_final_sequence=False, is_foul=False)), V(0)),
                final_sequence_potted = Coalesce(Count('id', filter=Q(is_final_sequence=True, is_foul=False)), V(0)),
                fouls_received = Coalesce(Count('id', filter=Q(is_foul=True)), V(0)),
            )
        return {}

    def _points_statistics(self):
        if hasattr(self, 'balls'):
            return self.balls.aggregate(
                total_points = Coalesce(Sum('value'), V(0)),
                reds_points = Coalesce(Sum('value', filter=Q(value=1, is_final_sequence=False, is_foul=False, is_free_ball=False)), V(0)),
                color_after_red_points = Coalesce(Sum('value', filter=Q(value__gt=1, is_final_sequence=False, is_foul=False, is_free_ball=False)), V(0)),
                average_color_after_red_points = Round(Coalesce(Avg('value', filter=Q(value__gt=1, is_final_sequence=False, is_foul=False, is_free_ball=False)), V(0.0))),
                free_ball_points = Coalesce(Sum('value', filter=Q(is_free_ball=True)), V(0)),
                average_free_ball_points = Round(Coalesce(Avg('value', filter=Q(is_free_ball=True)), V(0.0))),
                final_sequence_points = Coalesce(Sum('value', filter=Q(is_final_sequence=True)), V(0)),
                average_final_sequence_points = Round(Coalesce(Avg('value', filter=Q(is_final_sequence=True)), V(0.0))),
                fouls_received_points = Coalesce(Sum('value', filter=Q(is_foul=True)), V(0)),
                average_fouls_received_points = Round(Coalesce(Avg('value', filter=Q(is_foul=True)), V(0.0))),
                manual_points = Coalesce(Sum('value', filter=Q(is_manual=True)), V(0)),
            )


    @property
    def statistics(self):
        return {
            'points': self._points_statistics(),
            'breaks': self._break_statistics(), 
            'shots': self._shot_statistics(), 
        }
    
    @property
    def performance(self):
        potting = self._shot_statistics()['shots_potted'] / self._shot_statistics()['total_shots']
        positional_play = self.breaks.annotate(num_balls=Coalesce(Count('balls'),V(0))).aggregate(
            balls=Coalesce(Sum('num_balls', filter=Q(num_balls__gt=2))-Sum(2, filter=Q(num_balls__gt=2)),V(0))
        )['balls'] / self._shot_statistics()['total_shots']
        safety = self.frame.breaks.filter(~Q(team=self.team), score=0).count() / self.breaks.count()

        return {
            'potting': potting,
            'positional_play': positional_play,
            'safety' : safety,
        }

    
    class Meta:
        abstract = True


class Ball(models.Model):
    def __str__(self):
        return "{} de valor {}".format(self.types if self.types else "Bola", self.value)

    @property
    def types(self):
        if self.is_free_ball:
            return "Free Ball"
        if self.is_foul:
            return "Falta"
        if self.is_red_illegal_pot:
            return "Vermelha embolsada em falta"
        if self.is_end_break:
            return "Fim de break"
        if self.is_end_frame:
            return "Fim de frame"
        return ""

    @property
    def str_type(self):
        if self.is_free_ball:
            return "FB"
        if self.is_foul:
            return "F"
        if self.is_red_illegal_pot:
            return "-1"
        if self.is_end_break:
            return "EB"
        if self.is_end_frame:
            return "EF"
        return ""

    @property
    def rating(self):
        return self.value / 7

    @property
    def frame(self):
        return self.brk.frame

    brk =  models.ForeignKey("Break", related_name='balls', on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)

    value = models.PositiveSmallIntegerField(default=0)

    is_end_break = models.BooleanField(default=False, null = True, blank = True)
    is_end_frame = models.BooleanField(default=False, null = True, blank = True)
    is_free_ball = models.BooleanField(default=False, null = True, blank = True)
    is_foul = models.BooleanField(default=False, null = True, blank = True)
    is_manual = models.BooleanField(default=False, null = True, blank = True)
    is_red_illegal_pot = models.BooleanField(default=False, null = True, blank = True)
    is_last_red = models.BooleanField(default=False, null = True, blank = True)
    is_final_sequence = models.BooleanField(default=False, null = True, blank = True)

    created_at = models.DateTimeField(default=timezone.now, blank=True)
    shot_time = models.PositiveSmallIntegerField(null=True, blank=True)

    class Meta:
        verbose_name = "Ball/Shot"
        verbose_name_plural = "Balls/Shots"


class Break(models.Model):
    def __str__(self):
        return "Frame {} - Break {}".format(self.frame.id, self.order)

    @property
    def order(self):
        return self.frame.breaks.filter(id__lte = self.id).count()
    
    @property
    def frame_score(self):
        frame_score = self.frame.breaks.filter(id__lte = self.id).aggregate(
            team_1 = Coalesce(Sum('score', filter = Q(team = self.frame.team_profiles.first().team)), V(0)),
            team_2 = Coalesce(Sum('score', filter = Q(team = self.frame.team_profiles.last().team)), V(0)),
        )  
        return [frame_score['team_1'], frame_score['team_2']]

    frame = models.ForeignKey("Frame", related_name='breaks', on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    start_time = models.DateTimeField(default=timezone.now, null = True, blank = True, verbose_name="Início")
    end_time = models.DateTimeField(null = True, blank = True, verbose_name="Fim")
    score = models.PositiveSmallIntegerField(default=0)

class Frame(Statistics):
    def __str__(self):
        return "Match {} - Frame {}".format(self.match.id, self.frame_match_order())

    def frame_match_order(self):
        return self.match.frames.filter(id__lt = self.id).count() + 1

    @property
    def balls(self):
        return Ball.objects.filter(brk__in=self.breaks.all())

    @property
    def teams(self):
        return [
            self.team_profiles.filter(team=self.match.teams.first().team).first().team, 
            self.team_profiles.filter(team=self.match.teams.last().team).first().team
        ]
    
    @property
    def score(self):
        return [
            self.team_profiles.filter(team=self.match.teams.first().team).first().team_score, 
            self.team_profiles.filter(team=self.match.teams.last().team).first().team_score
        ]

    @property
    def rating(self):
        if self.winner:
            return max((1 - self.score_difference / 147, 0))
        return 0 

    @property
    def score_difference(self):
        return abs(self.score[0] - self.score[1])

    @property
    def order(self):
        return self.match.frames.filter(start_time__lte = self.start_time).count()

    match = models.ForeignKey("Match", on_delete=models.CASCADE, related_name='frames', verbose_name="Jogo")
    winner = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Vencedor", related_name="frame_wins")
    loser = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Perdedor", related_name="frame_losses")
    start_time = models.DateTimeField(default=timezone.now, null = True, blank = True, verbose_name="Início")
    end_time = models.DateTimeField(null = True, blank = True, verbose_name="Fim")

    class Meta:
        verbose_name = "Frame"
        verbose_name_plural = "Frames"


class Match(Statistics):
    def __str__(self):
        try:
            return "Match {} - {} x {}".format(self.id, self.teams.first().team, self.teams.last().team)
        except:
            return "Match %s" % (self.id,)

    @property
    def duration(self):
        end = self.end_time
        start = self.start_time
        if not end:
            end = timezone.now()
        if not start:
            start = timezone.now()
        return (end - start).seconds

    @property
    def balls(self):
        return Ball.objects.filter(brk__in=self.breaks)

    @property
    def breaks(self):
        return Break.objects.filter(frame__in=self.frames.all())


    competition_phase = models.ForeignKey(Phase, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Competição")
    winner = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Vencedor", related_name="match_wins")
    loser = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Perdedor", related_name="match_losses")
    best_of = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name="À melhor de")
    total_reds = models.PositiveSmallIntegerField(default=15, null=True, blank=True, verbose_name="Vermelhas")
    start_time = models.DateTimeField(default=timezone.now, null=True, blank=True, verbose_name="Início")
    end_time = models.DateTimeField(null = True, blank = True, verbose_name="Fim")
    scoreboard_data = models.TextField(null = True, blank = True)

    class Meta:
        verbose_name = "Jogo"
        verbose_name_plural = "Jogos"
        ordering = ['-start_time', '-id']

    def clean(self):
        # Don't allow more than two players
        if self.teams.count() > 2:
            raise ValidationError(_('A match can only be played between two players or teams.'))


class MatchTeam(Statistics):
    def __str__(self):
        return "Match {} - {}".format(self.match.id, self.team)

    @property
    def first_name(self):
        return self.team.first_name

    @property
    def last_name(self):
        return self.team.last_name

    @property
    def name(self):
        return self.team.name

    @property
    def frames(self):
        return self.match.frames.filter(team=self.team)

    @property
    def breaks(self):
        return self.match.breaks.filter(team=self.team)

    @property
    def balls(self):
        return self.match.balls.filter(profile__in=self.team.profiles.all())

    match = models.ForeignKey(Match, on_delete=models.CASCADE, related_name="teams", verbose_name="Jogo")
    team = models.ForeignKey(Team, on_delete=models.CASCADE, verbose_name="Equipa")
    score = models.PositiveSmallIntegerField(default=0)


class FrameTeamProfile(Statistics):
    def __str__(self):
        return "{} - {}".format(self.frame, self.team)

    @property
    def breaks(self):
        return self.frame.breaks.filter(team=self.team)

    @property
    def balls(self):
        return self.frame.balls.filter(profile__in=self.team.profiles.all())

    @property
    def name(self):
        return self.team.name

    frame = models.ForeignKey(Frame, on_delete=models.CASCADE, related_name="team_profiles", verbose_name="Frame")
    team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="frame_profiles", verbose_name="Equipa")
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name="frame_teams", verbose_name="Perfil")
    team_score = models.PositiveSmallIntegerField(default=0)
    profile_score = models.PositiveSmallIntegerField(default=0)


# @receiver(pre_save, sender=Match)
# def normalize_match(sender, instance, **kwargs):
#     if instance.id is not None:
#         previous = Match.objects.get(id=instance.id)
#         previous_end_time = None
#         if previous.end_time:
#             previous_end_time = previous.end_time.replace(microsecond=0)
#         if previous_end_time != instance.end_time: # field will be updated
#             instance.normalize_to_database()
