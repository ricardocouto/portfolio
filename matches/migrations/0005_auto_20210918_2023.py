# Generated by Django 3.2.7 on 2021-09-18 20:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matches', '0004_auto_20210918_1740'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ball',
            old_name='end_break',
            new_name='is_end_break',
        ),
        migrations.RenameField(
            model_name='ball',
            old_name='end_frame',
            new_name='is_end_frame',
        ),
        migrations.RenameField(
            model_name='ball',
            old_name='foul',
            new_name='is_foul',
        ),
        migrations.RenameField(
            model_name='ball',
            old_name='free_ball',
            new_name='is_free_ball',
        ),
        migrations.RenameField(
            model_name='ball',
            old_name='red_illegal_pot',
            new_name='is_red_illegal_pot',
        ),
    ]
