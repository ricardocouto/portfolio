# Generated by Django 3.2.7 on 2021-09-18 15:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0003_profile_teams'),
        ('matches', '0002_auto_20210918_1503'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ball',
            name='team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='profiles.team'),
        ),
        migrations.AlterField(
            model_name='frame',
            name='winner',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='profiles.team', verbose_name='Vencedor'),
        ),
        migrations.AlterField(
            model_name='frameprofileteam',
            name='frame',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='matches.frame', verbose_name='Frame'),
        ),
        migrations.AlterField(
            model_name='frameprofileteam',
            name='profile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='profiles.profile', verbose_name='Perfil'),
        ),
        migrations.AlterField(
            model_name='frameprofileteam',
            name='team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='profiles.team', verbose_name='Equipa'),
        ),
        migrations.AlterField(
            model_name='match',
            name='winner',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='profiles.team', verbose_name='Vencedor'),
        ),
        migrations.AlterField(
            model_name='matchteam',
            name='match',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='matches.match', verbose_name='Jogo'),
        ),
        migrations.AlterField(
            model_name='matchteam',
            name='team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='profiles.team', verbose_name='Equipa'),
        ),
    ]
