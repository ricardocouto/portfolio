from django.core.management.base import BaseCommand, CommandError
from matches.models import Match

class Command(BaseCommand):
    help = 'Deletes all matches'

    def handle(self, *args, **options):
        Match.objects.all().delete()
        self.stdout.write(self.style.SUCCESS('Deleted all matches!'))