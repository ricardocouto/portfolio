from django.core.management.base import BaseCommand, CommandError
from matches.models import Match
from profiles.models import Team

import trueskill
import itertools
import math
import functools


def win_probability(team1, team2):
    delta_mu = sum(r.mu for r in team1) - sum(r.mu for r in team2)
    sum_sigma = sum(r.sigma ** 2 for r in itertools.chain(team1, team2))
    size = len(team1) + len(team2)
    denom = math.sqrt(size * (trueskill.BETA * trueskill.BETA) + sum_sigma)
    ts = trueskill.global_env()
    trueskill.setup(draw_probability=0)
    return ts.cdf(delta_mu / denom)


def min_max(x, minx=0, maxx=1, minv=0, maxv=1):
    return (maxv - minv) / (maxx - minx) * (x - minx) + minv

class Command(BaseCommand):
    help = 'Calculates Elo for all players'

    def handle(self, *args, **options):
        matches = Match.objects.all()
        teams = Team.objects.all()

        team_stats = {}

        for team in teams:
            team_stats[team.name] = {
                'rating': trueskill.Rating(),
                'performance': {},
                'matches': 0,
            }

        for match in matches:
            win_prob_team_0 = win_probability([match_teams[0]['rating']], [match_teams[1]['rating']])
            perf_multiplier = [
                min_max((1 - win_prob_team_0), minv=0.75,maxv=1.25),
                min_max((win_prob_team_0), minv=0.75,maxv=1.25),
            ]

            for frame in match.frames.all():
                match_teams = [
                    team_stats[match.winner.name],         
                    team_stats[match.loser.name],
                ]


                team_stats[match.teams.first().team.name]['rating'], team_stats[match.teams.last().team.name]['rating'] = trueskill.rate_1vs1(match_teams[0]['rating'], match_teams[1]['rating'])

                for team, i in enumerate(match.teams.all()):
                    x = frame.team_profiles.filter(team=team.team).first().performance
                    team_stats[team.name]['performance'] = {k: x.get(k, 0) + perf_multiplier[i] * team_stats[team.name]['performance'].get(k, 0) for k in set(x)}
                    team_stats[team.name]['matches'] += 1

        for team in teams:
            team_stats[team.name]['performance'] = {k: round(v / team_stats[team.name]['matches'],3) for (k, v) in team_stats[team.name]['performance'].items()}


        for team in team_stats:
            print(team, team_stats[team])