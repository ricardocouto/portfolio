from django.core.management.base import BaseCommand, CommandError
from matches.models import Match, Frame, Ball, MatchTeam, FrameTeamProfile, Break
from profiles.models import Team
from django.utils import timezone

import random

class Command(BaseCommand):
    help = 'Generates random match'

    def add_arguments(self, parser):
        parser.add_argument('amount', nargs='+', type=int)

    def handle(self, *args, **options):
        for amount in range(options['amount'][0]):
            best_of = random.randint(1, 3) * 2 + 1
            team_list = Team.objects.order_by('?')

            match = Match.objects.create(
                best_of = best_of,
            )

            teams = [
                team_list.first(),
                team_list.last()
            ]

            match_teams = []

            for team in teams:
                match_teams.append(
                    MatchTeam.objects.create(
                        match = match,
                        team = team
                    )
                )
                
            is_game_won = False
            
            while not is_game_won:
                frame = Frame.objects.create(
                    match = match
                )

                brk = Break.objects.create(
                    frame = frame, 
                    team = teams[0],
                )

                frame_teams = []

                for team in teams:
                    frame_teams.append(
                        FrameTeamProfile.objects.create(
                            frame = frame, 
                            team = team,
                            profile = team.profiles.first()
                        )
                    )

                is_end_frame = False

                break_id = 0
                break_order = 0
                break_score = 0
                reds_in_table = 15
                final_sequence_value = 2

                while not is_end_frame:
                    is_end_break = random.choice(
                        [
                            True,
                            False,
                            False,
                        ]
                    )

                    team_index = break_id % 2
                    opposite_team_index = (break_id + 1) % 2

                    team = teams[team_index]

                    frame_team = next(filter(lambda t: t.team.id == team.id, frame_teams))

                    profile = team.profiles.first()

                    if not is_end_break:
                        if break_order % 2 == 0 and reds_in_table > 0:
                            value = 1
                            reds_in_table -= 1
                        elif reds_in_table > 0:
                            value = random.randint(2, 7)
                        else:
                            value = final_sequence_value
                            final_sequence_value += 1
                    else:
                        value = 0

                    break_score += value
                    break_order += 1
                        
                    Ball.objects.create(
                        brk = brk,
                        profile = profile,
                        value = value,
                        is_end_break = is_end_break,
                        is_last_red = (reds_in_table == 0),
                        is_final_sequence = (reds_in_table == 0 and value > 1)
                    )

                    frame_team.team_score += value
                    frame_team.profile_score += value

                    if is_end_break:
                        brk.score = break_score
                        brk.end_time = timezone.now()                        
                        brk.save()

                        break_id += 1
                        break_order = 0
                        break_score = 0

                        brk = Break.objects.create(
                            frame = frame, 
                            team = teams[opposite_team_index],
                        )
                        
                    if final_sequence_value > 7:
                        is_end_frame = True
                        brk.score = break_score
                        brk.end_time = timezone.now()                        
                        brk.save()

                    frame.end_time = timezone.now()

                if frame_teams[0].team_score > frame_teams[1].team_score:
                    frame.winner = teams[0]
                    frame.loser = teams[1]
                    match_teams[0].score += 1
                elif frame_teams[1].team_score > frame_teams[0].team_score:
                    frame.winner = teams[1]
                    frame.loser = teams[0]
                    match_teams[1].score += 1
                else:
                    Ball.objects.create(
                        brk = brk,
                        profile = profile,
                        value = value,
                        is_end_break = is_end_break,
                        is_last_red = (reds_in_table == 0),
                        is_final_sequence = (reds_in_table == 0 and value > 1)
                    )
                    frame_team.team_score += value
                    frame_team.profile_score += value
                    
                    frame.winner = team
                    match_team = next(filter(lambda t: t.team.id == team.id, match_teams))

                    frame.loser = teams[0]
                    if team.id == teams[0].id:
                        frame.loser = teams[1]

                    match_team.score += 1


                Ball.objects.create(
                    brk = brk,
                    profile = profile,
                    value = 0,
                    is_end_frame = is_end_frame
                )

                               
                frame.save()

                for frame_team in frame_teams:
                    frame_team.save()

                if match_teams[0].score > (best_of / 2):
                    match.winner = teams[0]
                    match.loser = teams[1]
                    is_game_won = True
                if match_teams[1].score > (best_of / 2):
                    match.winner = teams[1]
                    match.loser = teams[0]
                    is_game_won = True

                match.end_time = timezone.now()

            for match_team in match_teams:
                match_team.save()

            match.save()

            self.stdout.write(self.style.SUCCESS('Match %s created' % (match.id)))