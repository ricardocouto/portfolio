from matches.models import Match, Frame, Break, Ball, MatchTeam, FrameTeamProfile
from profiles.models import Profile, Team
from profiles.views import AuthModelViewSet
from rest_framework import serializers, viewsets
from backend.helpers import DefaultSerializer
from django.http import Http404


# Serializers define the API representation.
class ProfileSerializer(DefaultSerializer):
    class Meta:
        model = Profile
        fields = '__all__'


class TeamSerializer(DefaultSerializer):
    class Meta:
        model = Team
        fields = '__all__'


class BreakFrameSerializer(DefaultSerializer):
    class Meta:
        model = Frame
        fields = ('id', 'order')

class BallBreakSerializer(DefaultSerializer):
    team = TeamSerializer()
    frame = BreakFrameSerializer()
    order = serializers.IntegerField()

    class Meta:
        model = Break
        fields = '__all__'
        depth = 1

class MatchTeamSerializer(DefaultSerializer):
    statistics = serializers.DictField(required=False)
    name = serializers.CharField(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)

    class Meta:
        model = MatchTeam
        fields = '__all__'
        read_only_fields = ('first_name', 'last_name')
        extra_kwargs = {
            'score': {
                'required': False, 
                'allow_null': True
            },
            'match': {
                'required': False,
                'allow_null': True,
            }
        }


class FrameTeamProfileSerializer(DefaultSerializer):
    statistics = serializers.DictField()
    name = serializers.CharField()
    performance = serializers.DictField()

    class Meta:
        model = FrameTeamProfile
        fields = '__all__'


class BallSerializer(DefaultSerializer):
    brk = BallBreakSerializer()

    class Meta:
        model = Ball
        fields = '__all__'
        depth = 1


class BreakBallSerializer(BallSerializer):
    profile = ProfileSerializer()
    brk = None

    class Meta:
        model = Ball
        exclude = ('brk',)
        depth = 1


class BreakSerializer(DefaultSerializer):
    balls = BreakBallSerializer(many=True)
    team = TeamSerializer()
    frame = BreakFrameSerializer()
    order = serializers.IntegerField()
    frame_score = serializers.ListField(
        child=serializers.IntegerField()
    )
    
    class Meta:
        model = Break
        fields = '__all__'
        depth = 1


class FrameBreakSerializer(BreakSerializer):
    frame = None
    class Meta:
        model = Break
        exclude = ['frame']
        depth = 1


class FrameSerializer(DefaultSerializer):
    breaks = FrameBreakSerializer(many=True)
    order = serializers.IntegerField()
    team_profiles = FrameTeamProfileSerializer(many=True)
    winner = TeamSerializer()
    loser = TeamSerializer()
    teams = TeamSerializer(many=True)
    highest_break = BreakSerializer(required=False)
    top_shot = BallSerializer(required=False)
    analysis = serializers.DictField(required=False)

    class Meta:
        model = Frame
        fields = '__all__'
        depth = 1


class MatchFrameSerializer(FrameSerializer):
    class Meta:
        model = Frame
        exclude = ['match']


class MatchSerializer(DefaultSerializer):
    frames = MatchFrameSerializer(many=True, required=False)
    teams = MatchTeamSerializer(many=True, required=False)
    duration = serializers.IntegerField(required=False)
    best_frame = FrameSerializer(required=False)
    highest_break = BreakSerializer(required=False)
    top_shot = BallSerializer(required=False)
    analysis = serializers.DictField(required=False)
    winner = TeamSerializer()
    loser = TeamSerializer()

    class Meta:
        model = Match
        fields = '__all__'
        depth = 1

    def create(self, validated_data):
        match_teams = validated_data.pop('teams')
        match = Match.objects.create(**validated_data)

        try:
            for match_team in match_teams:
                MatchTeam.objects.create(match=match, team=match_team['team'])
            return match
        except:
            match.delete()
            raise Http404("Player/Team does not exist")


# ViewSets define the view behavior.
class MatchViewSet(AuthModelViewSet):
    def get_queryset(self, *args, **kwargs):
        #return Match.objects.filter(teams__team__in=self.valid_teams()).distinct().select_related('winner', 'loser').prefetch_related('frames', 'teams')
        return Match.objects.select_related('winner', 'loser').prefetch_related('frames', 'teams')

    serializer_class = MatchSerializer


class FrameViewSet(viewsets.ModelViewSet):
    queryset = Frame.objects.select_related('match', 'winner', 'loser').prefetch_related('breaks', 'team_profiles').all()
    serializer_class = FrameSerializer


class BreakViewSet(viewsets.ModelViewSet):
    queryset = Break.objects.select_related('frame', 'team').prefetch_related('balls').all()
    serializer_class = BreakSerializer


class BallViewSet(viewsets.ModelViewSet):
    queryset = Ball.objects.select_related('brk', 'profile').all()
    serializer_class = BallSerializer


class MatchTeamViewSet(viewsets.ModelViewSet):
    queryset = MatchTeam.objects.select_related('team', 'match').all()
    serializer_class = MatchTeamSerializer


class FrameTeamProfileViewSet(viewsets.ModelViewSet):
    queryset = FrameTeamProfile.objects.select_related('frame', 'team', 'profile').all()
    serializer_class = FrameTeamProfileSerializer